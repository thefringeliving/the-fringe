The Fringe are newly constructed apartments in Lawrence, KS that offer you country living in the heart of the city. Comfortable, open floor plans give our Lawrence, Kansas apartments all the luxuries you need to live a life of leisure & satisfaction. Our staff will make you feel right at home.

Address: 1525 Birdie Way, Lawrence, KS 66047, USA

Phone: 785-856-2147
